// Game.rooms['W7N5'].createConstructionSite(24,27,STRUCTURE_EXTENSION)
//Game.spawns['Spawn1'].createCreep([WORK, MOVE, CARRY], 'harvester1', {role: 'builder'})
//Game.spawns['Spawn1'].createCreep([WORK, MOVE, CARRY], 'harvester', {role: 'harvest'})
// Game.spawns['Spawn1'].createCreep([WORK, MOVE, MOVE, MOVE, CARRY], 'tester', {role: 'test'})
// Game.creeps['tester'].moveTo(new RoomPosition(28,8,"W8N4"));
  /*
Memory.sources = [];
Memory.sources.push({id: '33bd077274d064f', max: 2, room: 'W9N7', pos: [44, 23]});
            
Memory.targets = { upgrade: '6aa0077274df11c' }
Memory.targets.takeControl = {id: 'cccc0774ad79184', room: 'W2N1', pos: [7,11]}

require('help.spawn').spawn('claim', false);
require('conso').countRoles()

for(let c in Memory.creeps) { let creep = Game.creeps[c]; console.log(creep.memory.role)}
*/


module.exports.loop = () => { 
    var list_harvester = [];
    var active_creeps_build = 0;
    var active_creeps_harvest = 0;
    var active_creeps_claim = 0;
    var active_creeps_attack = 0;
    var active_creeps = 0;
    
    for(var id in Game.creeps){
        var creep = Game.creeps[id];
        active_creeps++;
        switch(creep.memory.role){
            case 'build': 
                active_creeps_build++;
            case 'harvest': 
                active_creeps_harvest++;
                break;
            case 'claim': active_creeps_claim++; break;
            case 'attack':
                active_creeps_attack++; 
                require('creep.handler').runAttacker(creep);
                continue;
            break;
        }
        require('creep.handler').runHarvester(creep);   
    }
    let spawn1 = Game.spawns['Spawn1'];
    new RoomVisual(spawn1.room.name)
        .text('Builder: ' + active_creeps_build, 1, spawn1.pos.y, 
        {color: 'green', font: 0.4, align: 'left'});
    new RoomVisual(spawn1.room.name)
        .text('Harvester: ' + active_creeps_harvest, 1, spawn1.pos.y+0.6, 
        {color: 'green', font: 0.4, align: 'left'});
    new RoomVisual(spawn1.room.name)
        .text('Attacker: ' + active_creeps_attack, 1, spawn1.pos.y+1.2, 
        {color: 'green', font: 0.4, align: 'left'});
    
    for(var room in Game.rooms){
        require('creep.handler').defendRoom(room)
    }
        
    if(!Game.spawns['Spawn1'].spawning){
        let creeps_harvest_need = 0;
        Memory.sources.forEach(source => creeps_harvest_need += source.max);
        if(active_creeps_harvest < 5){
            require('help.spawn').spawn('harvest', true);
        }else if(Memory.targets.takeControl
            && active_creeps_claim == 10){
            require('help.spawn').spawn('claim', false);
        }else if(active_creeps_attack < active_creeps / 4){
            require('help.spawn').spawn('attack', false);
        }else if(active_creeps_harvest < creeps_harvest_need){
            if(Math.random() < 0.3)
                require('help.spawn').spawn('build', false);
            else
                require('help.spawn').spawn('harvest', false);
        }
    }
}