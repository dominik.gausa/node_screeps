/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('helpers');
 * mod.thing == 'a thing'; // true
 */
// require("harvest.getFree").update();

module.exports = {
    update: function (){
        console.log("Updating Sources")
        for(var id in Memory.sources){
            if(typeof Memory.sources[id].room === 'undefined'){
                var s = Game.getObjectById(Memory.sources[id].id);
                console.log('Update ' + id + ' in room ' + s.pos.roomName)
                Memory.sources[id].room = s.pos.roomName;
                Memory.sources[id].x = s.pos.x;
                Memory.sources[id].y = s.pos.y;
            }
        }
        for(var id in Game.creeps){
            var creep = Game.creeps[id];
            console.log(id)
            for(var sId in Memory.sources){
                if(creep.memory.source == Memory.sources[sId].id){
                    creep.memory.id = sId;
                    console.log('checked ' + id + ' Source ' + sId);
                }
            }
        }
    },
    run: function (){
        console.log("Updating Sources")
        for(var id in Memory.sources){
            var count = 0
            var source = Memory.sources[id];
                
            for(var name in Game.creeps){
                var creep = Game.creeps[name]
                if(creep.memory.source == id)
                    count++;
            }
            if(count < source.max){
                console.log(id + " Ok")
                return id;
            }
        }
        return 'undefined'
    },
    
    cleanMemoryCreep: () => {
        Object.keys(Memory.creeps).forEach((k) => {
            for(let creep in Game.creeps){
               if(creep == k) 
                return;
            }
            console.log('Delete ', k, ' from Memory');
            delete Memory.creeps[k];
        });
    },
    
    countRoles: () => {
        let obj = {};
        Object.keys(Game.creeps).forEach(k => {
            let creep = Game.creeps[k];
            if(!obj[creep.memory.role])
                obj[creep.memory.role] = 0
            obj[creep.memory.role]++;
        });
        return JSON.stringify(obj);
    }
};