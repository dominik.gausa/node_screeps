/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('helpers');
 * mod.thing == 'a thing'; // true
 */
// require('help.spawn').spawn('harvest')


var workerSize_0 = [].concat(
    Array(1).fill(WORK),
    Array(1).fill(MOVE),
    Array(1).fill(CARRY)
    );


module.exports = class HELPER {
    static spawn (destRole, tiny) {
        var extensions = Game.spawns['Spawn1'].room.find(FIND_MY_STRUCTURES, {
                filter: { structureType: STRUCTURE_EXTENSION }
            });
            
        let creepBigness = extensions.length/2;
        if(tiny === undefined
            || tiny == true)
            creepBigness = 0;
        
        let creepSize;
        switch(destRole){
        case 'build':
        case 'harvest':
            creepSize = [].concat(
                Array(Math.floor(1 + creepBigness*0.3)).fill(WORK),
                Array(Math.floor(1 + creepBigness*0.3)).fill(MOVE),
                Array(Math.floor(1 + creepBigness*0.4)).fill(CARRY)
            );
            break;
            
        case 'attack':
            creepSize = [].concat(
                Array(Math.floor(1 + creepBigness*0.3)).fill(ATTACK),
                Array(Math.floor(1 + creepBigness*0.3)).fill(MOVE),
                Array(Math.floor(1 + creepBigness*0.4)).fill(TOUGH)
            );
            break;
            
        case 'claim':
            creepSize = [CLAIM, MOVE, WORK, CARRY];
            break;
        }
            
        var i = 0;
        let ret = 0;
        do{
            ret = Game.spawns['Spawn1'].spawnCreep(creepSize, destRole + i, {memory:{role: destRole}});
            i++;
        }while(ret == ERR_NAME_EXISTS);
        //console.log('Ret: ', ret);
        if(ret == 0){
            console.log("Spawning: " + destRole);
            require('conso').cleanMemoryCreep();
        }
    }
    
    static run (creepName){
        var creep = Game.creeps[creepName];
        creep.memory.source = Memory.sources[Math.floor(Memory.sources.length*Math.random())];
        creep.memory.state = 0;
    }
};