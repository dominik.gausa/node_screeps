//Game.spawns['Spawn1'].spawnCreep([WORK, CARRY, MOVE], 'Worker1');

function isSpawnInList(id){
    let inList = false;
    Memory.sources.forEach(s => {
        if(s.id == id)
            inList = true;
    })
    return inList;
}

function checkSlots(room, pos){
    let slots = 0;
    let things = room.lookAtArea(pos.y-1, pos.x-1, pos.y+1, pos.x+1);
    //console.log(JSON.stringify(things));
    //console.log('Y:' + Object.keys(things).length);
    //console.log('X:' + Object.keys(things[Object.keys(things)[0]]).length);
    Object.keys(things).forEach(k => {
        let row = things[k];
        Object.keys(row).forEach(k => {
            let square = row[k];
            square.forEach(e => {
               if(e.terrain == 'plain') slots++;
            });
        });
    });
    return slots;
}

function getSources(){
    if(!Memory.sources){
        Memory.sources = [];
    }
    Object.keys(Game.spawns).forEach(key => {
        let spawn = Game.spawns[key];
        spawn.room.find(FIND_SOURCES).forEach(s => {
            if(!isSpawnInList(s.id)){
                console.log(s.id, s.pos);
                let slots = checkSlots(spawn.room, s.pos)
                console.log(slots);
                let source = {};
                source.id = s.id;
                source.pos = {x: s.pos.SPAWN_HITSx, y: s.pos.y};
                Memory.sources.push(source);
            }
        });
    });
}
module.exports.getSources = getSources;
