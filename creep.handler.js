/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('creep.handler');
 * mod.thing == 'a thing'; // true
 */

module.exports = class CreepHelper{
    static doBuild(creep){
        var targets = creep.room.find(FIND_CONSTRUCTION_SITES);
        if(!targets || targets.length == 0)
            return false;
            
        let closest = null;
        let dist = 1000000;
        for(let i = 0; i < targets.length; i++){
            let newDist = creep.pos.getRangeTo(targets[i])
            if(newDist >= dist)
                continue;
            dist = newDist;
            closest = targets[i];
        }
        if(creep.build(closest) == ERR_NOT_IN_RANGE) {
            creep.moveTo(closest, {reusePath: 3, visualizePathStyle: {stroke: '#ffffff'}});
        }
        return true;
    };
    
    static doClaim(creep){
        let memTarget = Memory.targets.takeControl;
        var target = Game.getObjectById(memTarget.id)
        if(!target){
            creep.moveTo(new RoomPosition(
                memTarget.pos[0], 
                memTarget.pos[1],
                memTarget.room),
                {reusePath: 3, visualizePathStyle: {stroke: '#ffaa00'}});
            return true;
        }
        if(!target.my){
            if(creep.claimController(target) == ERR_NOT_IN_RANGE) {
                if(creep.moveTo(target, {reusePath: 3, visualizePathStyle: {stroke: '#ffffff'}}) != 0)
                    return true;
            }
        }else{
            if(creep.upgradeController(target) == ERR_NOT_IN_RANGE) {
                if(creep.moveTo(target, {reusePath: 3, visualizePathStyle: {stroke: '#ffffff'}}) != 0)
                    return true;
            }
        }
        return true
    }
    
    static doUpgradeEmergency(creep){
        var target = Game.getObjectById(Memory.targets.upgrade)
        if(target.ticksToDowngrade > 300)
            return false;
        
        if(creep.upgradeController(target) == ERR_NOT_IN_RANGE) {
            if(creep.moveTo(target, {reusePath: 3, visualizePathStyle: {stroke: '#ffffff'}}) != 0)
                return true;
        }
        return true
    };
    

    static doUpgrade(creep){
        if(!Memory.targets.upgrade)
        return false;
        var target = Game.getObjectById(Memory.targets.upgrade)
        if(creep.upgradeController(target) == ERR_NOT_IN_RANGE) {
            if(creep.moveTo(target, {reusePath: 3, visualizePathStyle: {stroke: '#ffffff'}}) != 0)
                return false;
        }
        return true;
    }
    
    static returnEnergy_target(creep){
        var targets = creep.room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return (structure.structureType == STRUCTURE_EXTENSION ||
                        structure.structureType == STRUCTURE_SPAWN ||
                        structure.structureType == STRUCTURE_TOWER) && structure.energy < structure.energyCapacity;
            }
        });
        
        if(targets.length > 0) {
            var closest = targets[0];
            var dx = closest.pos.x - creep.pos.x
            var dy = closest.pos.y - creep.pos.y
            var dist = dx * dx + dy * dy;
            for(var tId in targets){
                var target = targets[tId];
                dx = target.pos.x - creep.pos.x
                dy = target.pos.y - creep.pos.y
                if(dist > dx * dx + dy * dy){
                    dist = dx * dx + dy * dy;
                    closest = target;
                }
            }
            creep.memory.targetEnergy = closest.id
            return true;
        } else
            return false;
    }
    
    static return_energy(creep){
        if(typeof creep.memory.targetEnergy === 'undefined'){
            if(!CreepHelper.returnEnergy_target(creep))
                return false;
        }
        
        var target = Game.getObjectById(creep.memory.targetEnergy);
        if(!target)
            return false; 
        if(target.energy == target.energyCapacityAvailable){
            if(!CreepHelper.returnEnergy_target(creep))
                return false;
            target = Game.getObjectById(creep.memory.targetEnergy);
        }
        
        var ret = creep.transfer(target, RESOURCE_ENERGY);
        switch(ret) {
            case ERR_NOT_IN_RANGE:
                creep.moveTo(target, {reusePath: 3, visualizePathStyle: {stroke: '#ffffff'}});
                break;
            case ERR_FULL:
                if(!CreepHelper.returnEnergy_target(creep))
                    return false;
                break;
            case 0:
                break;
            default:
                //console.log('retEner: ' + ret);
                delete creep.memory.targetEnergy;
                return false;
        }
        return true;
    }
    
    static doHarvest(creep){
        if(typeof creep.memory.source === 'undefined'){
            if(creep.memory.role == 'claim'){
                // Take Resource close to Target
                let memSource = Memory.targets.takeControl;
                let sources = [];
                Memory.sources.forEach((source, key) => {
                    if(source.room == memSource.room)
                        sources.push(key);
                });
                creep.memory.source = sources[Math.floor(Math.random() * sources.length)];
                
            }else{
                // Just fill holes
                creep.memory.source = require('conso').run();
            }
            console.log(creep.name + " is now harvesting from: " + creep.memory.source, ' in Room: ', Memory.sources[creep.memory.source].room);
        }
        try{
            let memSource = Memory.sources[creep.memory.source];
            var source = Game.getObjectById(memSource.id);
            if(source == null){
            //    console.log(JSON.stringify(memSource))
                creep.moveTo(new RoomPosition(
                    memSource.pos[0], 
                    memSource.pos[1],
                    memSource.room),
                    {reusePath: 3, visualizePathStyle: {stroke: '#ffaa00'}});
            }
        }catch(ex){
            console.log('ERROR: Loading Source for Creep: ', creep.name, ' Creeps Source: ', creep.memory.source);
            console.log(ex);
        }
        if(creep.harvest(source) == ERR_NOT_IN_RANGE){
            creep.moveTo(source, {visualizePathStyle: {stroke: '#ffaa00'}});
        }
    }
    
    static doRepair(creep){
        const targets = creep.room.find(FIND_STRUCTURES, {
            filter: object => {
                return (object.hits < object.hitsMax * 0.9)
                && (object.structureType != STRUCTURE_WALL 
                    || object.hits < object.hitsMax * 0.0001)}
        });
        targets.sort((a,b) => a.hits - b.hits);
        if(targets.length > 0) {
            if(creep.repair(targets[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(targets[0]);
            }
            return true;
        }
        return false;
    }
    
    static PrintState(creep, state){
        new RoomVisual(creep.room.name)
            .text(state, creep.pos.x, creep.pos.y+0.7,
                {color: 'green', font: 0.4});
    }
    
    static runHarvester(creep){
        switch(creep.memory.state)  {
        case 0:
            if(creep.carry.energy < creep.carryCapacity){
                return CreepHelper.doHarvest(creep);
            }else{
                creep.memory.state = 1;
                creep.say("Go to Work");
            }
            break;
        default:
            if(creep.pos.roomName != Game.spawns['Spawn1'].pos.roomName){
                creep.moveTo(Game.spawns['Spawn1'], {reusePath: 3});
                break;
            }
            switch(creep.memory.role){
            case 'build':
                switch(creep.memory.state){
                case 1:
                    CreepHelper.PrintState(creep, 'upEmer');
                    if(!CreepHelper.doUpgradeEmergency(creep))
                        creep.memory.state++;
                    break;
                case 2:
                    CreepHelper.PrintState(creep, 'Repair');
                    if(!CreepHelper.doRepair(creep))
                        creep.memory.state++;
                    break;
                case 3:
                    CreepHelper.PrintState(creep, 'Build');
                    if(!CreepHelper.doBuild(creep))
                        creep.memory.state++;
                    break;
                default:
                    CreepHelper.PrintState(creep, 'Upgrade');
                    CreepHelper.doUpgrade(creep)
                }
                break;
            case 'harvest':
                switch(creep.memory.state){
                case 1:
                    CreepHelper.PrintState(creep, 'retEner');
                    if(!CreepHelper.return_energy(creep))
                        creep.memory.state++;
                    break;
                default:
                    CreepHelper.PrintState(creep, 'Upgrade');
                    CreepHelper.doUpgrade(creep)
                }
                break;
            case 'claim':
                switch(creep.memory.state){
                default:
                    //creep.say("doClaim");
                    CreepHelper.doClaim(creep);
                }
                break;
            default:
                let prevRole = creep.memory.role;
                creep.memory.role = 'harvest';
                console.log(creep, ' Somehow didn\'t match any role. Setting it to ', creep.memory.role, ' (Prev: ', prevRole, ')');
            }
            if(creep.carry.energy == 0){
                creep.memory.state = 0;
                creep.say("Empty");
            }
            break;
        }
    }
    
    static runAttacker(creep){
        var hostilesStruct = Game.rooms[creep.room.name].find(FIND_HOSTILE_STRUCTURES, {
            filter: {structureType: STRUCTURE_TOWER}
        });
        var hostiles = Game.rooms[creep.room.name].find(FIND_HOSTILE_CREEPS);
        
        
        if(hostilesStruct.length > 0) {
            var username = hostiles[0].owner.username;
            hostilesStruct = hostiles.sort((a,b) => {return a.hits - b.hits;});
            
            if(creep.attack(hostilesStruct[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(hostilesStruct[0]);
            }
        }else if(hostiles.length > 0) {
            var username = hostiles[0].owner.username;
            hostiles = hostiles.sort((a,b) => {return a.hits - b.hits;});
            
            if(creep.attack(hostiles[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(hostiles[0]);
            }
        }else if(Game.flags.gathering){
            creep.moveTo(Game.flags.gathering, {visualizePathStyle: {stroke: '#ff0000'}});
        }
    }
    
    static defendRoom(roomName) {
        var hostiles = Game.rooms[roomName].find(FIND_HOSTILE_CREEPS);
        if(hostiles.length > 0) {
            var username = hostiles[0].owner.username;
            hostiles = hostiles.sort((a,b) => {return a.hits - b.hits;});
            Game.notify(`User ${username} spotted in room ${roomName}`);
            var towers = Game.rooms[roomName].find(
                FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}});
            towers.forEach(tower => tower.attack(hostiles[0]));
        }
    }
};